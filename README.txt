Views Datasource Collection README
-------------------------------------------------------------------------------

About
-----

This module extends Views Datasource to allow to inline Field Collections fields 
in JSON format views. 

In field collections in views with JSON style it offers an extra option to display
in the output field machine names or a field labels.


Known Issues
------------

- Field collections with multiple items are not rendered correctly.
