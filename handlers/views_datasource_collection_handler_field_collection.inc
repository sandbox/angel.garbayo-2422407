<?php
/**
 * @file
 * Implements render for field collection fields.
 */

class views_datasource_collection_handler_field_collection extends views_handler_field_field {

  /**
   * Implements option definition.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['use_field_label'] = array('default' => FALSE, 'bool' => TRUE);

    return $options;
  }

  /**
   * Option to use labels instead of machine name.
   */
  public function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);

    $form['use_field_label'] = array(
      '#title' => t('Use field labels instead of machine name'),
      '#description' => t('Output will use field labels instead of the field machine name.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['use_field_label']),
    );

  }

  /**
   * Render field collection.
   * 
   * @see views_handler_field
   */
  public function advanced_render($values) {
    if ($this->view->style_plugin->plugin_name != 'views_json') {
      return parent::advanced_render($values);
    }

    $output = array();

    $entities = $this->getEntities($values);
    foreach ($entities as $e) {
      $item = array();
      foreach ($e['#entity'] as $key => $field) {
        if (field_info_field($key)) {
          $field_name = ($this->options['use_field_label']) ? $e[$key]['#title'] : $key;
          $item[$field_name] = array_pop($field[LANGUAGE_NONE][0]);
        }
      }
      $output[] = $item;
    }

    if (count($output) == 1) {
      $output = array_pop($output);
    }

    return $output;
  }

  /**
   * Get entities to render.
   * @return array 
   *   Entities in value items
   */
  private function getEntities($values) {
    $raw_items = $this->get_items($values);
    $entities = array();
    foreach ($raw_items as $raw_item) {
      $r = array_pop($raw_item['rendered']['entity']['field_collection_item']);
      $entities[] = $r;
    }
    return $entities;
  }
}
