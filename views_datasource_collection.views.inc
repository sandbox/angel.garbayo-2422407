<?php
/**
 * @file
 * Hooks for views module.
 */


/**
 * Implements hook_views_data_alter().
 */
function views_datasource_collection_views_data_alter(&$data) {
  foreach ($data as $table => $config) {
    foreach ($config as $item => $item_config) {
      if (isset($item_config['field']['handler']) && $item_config['field']['handler'] == 'views_handler_field_field'
          && isset($config[$item . '_value']['relationship']['base']) && $config[$item . '_value']['relationship']['base'] == 'field_collection_item'
              ) {
        $data[$table][$item]['field']['handler'] = 'views_datasource_collection_handler_field_collection';
      }
    }
  }
  return $data;
}

/**
 * Implements hook_views_handlers().
 */
function views_datasource_collection_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_datasource_collection') . '/handlers',
    ),
    'handlers' => array(
      'views_datasource_collection_handler_field_collection' => array(
        'parent' => 'views_handler_field_field',
      ),
    ),
  );
}
